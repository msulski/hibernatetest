package pl.msulski.test.hibernate;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.hibernate.Session;
import org.hibernate.Transaction;

import pl.msulski.test.hibernate.model.Category;
import pl.msulski.test.hibernate.model.Stock;
import pl.msulski.test.hibernate.model.StockDailyRecord;

public class App {
	public static void main(String[] args) {
		System.out.println("Maven + Hibernate + MySQL");

		deleteData();

		oneToOneTest();
		oneToManyTest();
		manyToManyTest();
	}

	private static void deleteData() {
		Session session = HibernateUtil.getSessionFactory().openSession();

		session.beginTransaction();
		session.createQuery("delete from StockDailyRecord").executeUpdate();
		session.createSQLQuery("delete from stock_category").executeUpdate();
		session.createQuery("delete from Stock").executeUpdate();
		session.getTransaction().commit();
	}

	private static void oneToOneTest() {
		writeSampleData();
		readAndDisplaySampleData();
	}

	private static void readAndDisplaySampleData() {
		Session session = HibernateUtil.getSessionFactory().openSession();
		for (Object stock : session.createQuery("from Stock").list()) {
			System.out.println(((Stock) stock).getStockName());
		}
	}

	private static void writeSampleData() {
		Session session = HibernateUtil.getSessionFactory().openSession();

		session.beginTransaction();
		Stock stock = new Stock();

		stock.setStockCode("4715");
		stock.setStockName("GENM");

		session.save(stock);
		session.getTransaction().commit();
	}

	private static void manyToManyTest() {
		System.out.println("Hibernate many to many (Annotation)");
		Session session = HibernateUtil.getSessionFactory().openSession();

		session.beginTransaction();

		Stock stock = new Stock();
		stock.setStockCode("7053");
		stock.setStockName("PADINI");

		Category category1 = new Category("CONSUMER", "CONSUMER COMPANY");
		Category category2 = new Category("INVESTMENT", "INVESTMENT COMPANY");

		Set<Category> categories = new HashSet<Category>();
		categories.add(category1);
		categories.add(category2);

		stock.setCategories(categories);

		session.save(stock);

		session.getTransaction().commit();
		System.out.println("Done");
	}

	private static void oneToManyTest() {
		System.out.println("Hibernate one to many (Annotation)");
		Session session = HibernateUtil.getSessionFactory().openSession();

		session.beginTransaction();

		Stock stock = new Stock();
		stock.setStockCode("7052");
		stock.setStockName("PADINI2");
		session.save(stock);

		StockDailyRecord stockDailyRecords = new StockDailyRecord();
		stockDailyRecords.setPriceOpen(new Float("1.2"));
		stockDailyRecords.setPriceClose(new Float("1.1"));
		stockDailyRecords.setPriceChange(new Float("10.0"));
		stockDailyRecords.setVolume(3000000L);
		stockDailyRecords.setDate(new Date());

		stockDailyRecords.setStock(stock);
		stock.getStockDailyRecords().add(stockDailyRecords);

		session.save(stockDailyRecords);

		session.getTransaction().commit();
		System.out.println("Done");
	}

}

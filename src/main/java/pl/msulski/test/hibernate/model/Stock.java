package pl.msulski.test.hibernate.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "stock")
public class Stock {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "STOCK_ID")
	private Integer stockId;

	@Column(name = "STOCK_CODE", unique = true, nullable = false, length = 10)
	private String stockCode;

	@Column(name = "STOCK_NAME", unique = true, nullable = false, length = 20)
	private String stockName;

	@OneToOne(cascade = CascadeType.ALL)
	@PrimaryKeyJoinColumn
	private StockDetails1 stockDetails1;

	@OneToOne
	@JoinColumn(name = "stock_details2_fk")
	private StockDetails2 stockDetails2;

	@OneToOne
	private StockDetails3 stockDetails3;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "stock", cascade = CascadeType.ALL)
	private Set<StockDailyRecord> stockDailyRecords = new HashSet<StockDailyRecord>(0);

	@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinTable(name = "stock_category", 
		joinColumns = { @JoinColumn(name = "STOCK_ID", updatable = false) }, 
		inverseJoinColumns = { @JoinColumn(name = "CATEGORY_ID", nullable = false, updatable = false) })
	private Set<Category> categories = new HashSet<Category>(0);

	public Stock() {
	}

	public Set<Category> getCategories() {
		return categories;
	}

	public void setCategories(Set<Category> categories) {
		this.categories = categories;
	}

	public Stock(String stockCode, String stockName) {
		this.stockCode = stockCode;
		this.stockName = stockName;
	}

	public Integer getStockId() {
		return this.stockId;
	}

	public void setStockId(Integer stockId) {
		this.stockId = stockId;
	}

	public String getStockCode() {
		return this.stockCode;
	}

	public void setStockCode(String stockCode) {
		this.stockCode = stockCode;
	}

	public String getStockName() {
		return this.stockName;
	}

	public void setStockName(String stockName) {
		this.stockName = stockName;
	}

	public StockDetails1 getStockDetails() {
		return stockDetails1;
	}

	public void setStockDetails(StockDetails1 stockDetails) {
		this.stockDetails1 = stockDetails;
	}

	public Set<StockDailyRecord> getStockDailyRecords() {
		return stockDailyRecords;
	}

	public void setStockDailyRecords(Set<StockDailyRecord> stockDailyRecords) {
		this.stockDailyRecords = stockDailyRecords;
	}

}
